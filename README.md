# Demo to update Jira stories based on status of Cases #

This repository shows sample code based on Cucumber-Java framework to update Jira Stories based on results of executions.

### Quick Info ###

**[AIOCucumberEventListener](https://bitbucket.org/aio-tests-demo-examples/jira-update-cucumber-java/src/master/src/main/java/com/aiotests/core/cucumber/AIOCucumberEventListener.java)** is a Cucumber Event listener which is invoked at the start and end of a suite and each case.  It binds all the calls.

At the start of the run, a new cycle is created using AIO Tests API : [Create Cycle](https://tcms.aiojiraapps.com/aio-tcms/aiotcms-static/api-docs/#/Cycle%20creation/createTestCycleDetail).  
  
For this demo, the Cucumber testcases are annotated with Jira Stories.  
At the end of the testcase event, the associated Jira story is picked up for the testcase.  eg. [Feature file](https://bitbucket.org/aio-tests-demo-examples/jira-update-cucumber-java/src/master/src/test/resources/features/scenarios.feature)
  
`In case, you plan to update for a specific sprint or release, a Jira API can be invoked to find the list of stories. ` 
  
When the suite finishes, for all the stories gathered from the testcases or from the Jira API, the [AIO Tests API Get Traceability](https://tcms.aiojiraapps.com/aio-tcms/aiotcms-static/api-docs/#/Traceability/getTraceabilityForRequirement) is called.  
  
    
Based on the result of the Get Traceability API, if any case is found to be failing, Jira calls can be made for each story to transition the state of the story based on the organization's process.    
  
  * * *  
	for (String story : jiraStories) {
      	String issueId = JiraHelper.get().getIssueId(story);
      	if(AIOHelper.get().findIssueStatus(this.projectKey,issueId) == "Failed"){
	    	JiraHelper.get().moveToInProgress(issueId);
      	} else {
			JiraHelper.get().closeIssue(issueId);
	  	}
	}
  * * *	
	
**AIOHelper** [class](https://bitbucket.org/aio-tests-demo-examples/jira-update-cucumber-java/src/master/src/main/java/com/aiotests/core/rest/) details the AIO Tests API calls

**JiraHelper** [class](https://bitbucket.org/aio-tests-demo-examples/jira-update-cucumber-java/src/master/src/main/java/com/aiotests/core/rest/) details the Jira API calls




