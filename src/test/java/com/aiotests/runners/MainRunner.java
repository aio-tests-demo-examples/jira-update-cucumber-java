package com.aiotests.runners;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features = {"src/test/resources/features/"},
        glue = "com.aiotests.stepdefinitions",
        plugin = { "html:target/results.html",
                "com.aiotests.core.cucumber.AIOCucumberEventListener",
                "junit:target/cucumber-reports/Cucumber.xml",})
public class MainRunner extends AbstractTestNGCucumberTests {

}
