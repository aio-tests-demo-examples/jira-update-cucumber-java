package com.aiotests.stepdefinitions.forestcreator;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;

public class ForestCreatorStepDefs {

    @Given("I plant {int} trees")
    public void plantTrees(int numOfTrees) {
        System.out.println(numOfTrees);
        String[] someData = new String[]{"ads","dsdf"};
        List<String> somedataList = Arrays.asList(someData);
        System.out.println(somedataList.removeIf(s -> s.startsWith("x")));
        System.out.println(somedataList);
        System.out.println(somedataList.removeIf(s -> s.startsWith("a")));
        System.out.println(somedataList);
    }

    @Then("I should be proud")
    public void plantTrees() {
        System.out.println("I am proud");
    }

    @When("The forest has grown")
    public void grownForest() {
        System.out.println("The forest has grown");
    }

    @When("I eat fruits")
    public void eatFruits() {
        System.out.println("I et fruits");
    }

}
