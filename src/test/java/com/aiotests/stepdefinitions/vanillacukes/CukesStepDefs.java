package com.aiotests.stepdefinitions.vanillacukes;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class CukesStepDefs {
    @Given("I have {int} files to upload")
    public void i_have_n_cukes_in_my_belly(int cukes) {
        System.out.println(cukes);
    }

    @When("I multiselect and upload in one go")
    public void multiUpload() {
        System.out.println("Upload files");
    }

    @Then("Files get uploaded")
    public void assertFilesUploaded() {
        System.out.println("Upload files");
        Assert.assertEquals(true, false, "Files are uploaded");
    }
}
