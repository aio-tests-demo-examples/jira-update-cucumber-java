package com.aiotests.automation.samples;

import com.aiotests.core.rest.RestAPIDetail;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.io.File;
import java.util.*;

import static io.restassured.RestAssured.given;

public class AIORestAssuredExample {

    public static final String GET_CYCLE_DETAILS = "/project/{projectKey}/testcycle/{cycleKey}/detail";
    public static final String CREATE_CYCLE = "/project/{projectKey}/testcycle/detail";
    public static final String MARK_CASE = "project/{projectKey}/testcycle/{cycleKey}/testcase/{caseKey}/testrun?createNewRun={createNewRun}";
    public static final String IMPORT_RESULTS = "/project/{projectKey}/testcycle/{cycleKey}/import/results?type={type}";
    private static RequestSpecification defaultRequestSpec;

    /**
     *
     * @param args
     */
    public static void main(String[] args) {

        setupAIOConfig();

        String projectKey = "NVPROJ";
        String cycleKey = "NVPROJ-CY-37";
        //Get cycle details
        Response cycleDetails = doGet(GET_CYCLE_DETAILS, projectKey, cycleKey);
        System.out.println(cycleDetails.jsonPath().get("ID").toString());

        //Create cycle assuming gson or jackson is in dependencies
        String newCycleKey = createCycle(projectKey);

        //mark case status
        markCaseStatus(projectKey, newCycleKey, Arrays.asList("NVPROJ-TC-1","NVPROJ-TC-2"));

        importResults(projectKey, cycleKey,"target/TEST-ForestCreatorTests.xml", "Junit");

    }

    private static void importResults(String projectKey, String cycleKey, String fileName, String frameworkType) {
        Map<String, Object> formParams = new HashMap<>();
        formParams.put("createNewRun", true);
        formParams.put("addCaseToCycle", true);
        formParams.put("createCase", true);
        File f = new File(fileName);
        doMultipartPost(IMPORT_RESULTS, f, formParams, projectKey, cycleKey, frameworkType);
    }

    public static String createCycle(String projectKey) {
        Map<String, Object> newCycleDetails = new HashMap<>();
        newCycleDetails.put("title", "Regression Release 1");
        newCycleDetails.put("objective", "Trial Run");
        Map<String, Object> folderDetails = Collections.singletonMap("ID","213");
        newCycleDetails.put("folder", folderDetails);

        Response response = doPost(CREATE_CYCLE, newCycleDetails, projectKey);
        String newCycleKey = response.jsonPath().getString("key");
        System.out.println("New cycle created " + newCycleKey);
        return newCycleKey;
    }

    public static void markCaseStatus(String projectKey, String cycleKey, List<String> caseKeys) {
        Map<String, Object> runStatus = new HashMap<>();
        runStatus.put("testRunStatus", "Passed");
        runStatus.put("effort", "60000");
        for(String caseKey : caseKeys) {
            doPost(MARK_CASE, runStatus, projectKey, cycleKey, caseKey, "true" );
        }
    }


    public static Response doGet(String path, String ...pathParams) {
        Response response = given(defaultRequestSpec).when().get(path,pathParams).andReturn();
        response.prettyPrint();
        return response;
    }

    public static Response doPost(String path, Map<String, Object> params, Object ...pathParams) {
        Response response = given(defaultRequestSpec).contentType(ContentType.JSON).body(params).when().post(path,pathParams).andReturn();
        return response;
    }

    public static Response doMultipartPost(String path, File file, Map<String, Object> formParams, Object ...pathParams) {

        Response response = given(defaultRequestSpec).multiPart("file", file).formParams(formParams).when().post(path,pathParams).andReturn();
        response.prettyPrint();
        return response;
    }

    public static void setupAIOConfig() {
        RequestSpecBuilder builder   = new RequestSpecBuilder();
        builder.setBaseUri("https://tcms-dev.aioreports.com");
        builder.setBasePath("/aio-tcms/api/v1");
        builder.addHeader("Authorization", "AioAuth ODQyMTMwNDUtODdlYy00N2Q2LWJiOTItNzg3MzZmZTVmMDdh");
        builder.log(LogDetail.METHOD).log(LogDetail.URI);
        defaultRequestSpec = builder.build();
    }
}
