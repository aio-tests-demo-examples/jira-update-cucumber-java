package com.aiotests.core.rest;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AIOHelper extends APIHelper {
    static RequestSpecification defaultRequestSpec;
    private static AIOHelper aioHelper;
    private static RequestSpecBuilder builder;
    static {
        RestAssured.baseURI = "https://tcms-dev.aioreports.com";
        RestAssured.basePath = "/aio-tcms/api/v1";
        RequestSpecBuilder builder   = new RequestSpecBuilder();
        builder.addHeader("Authorization", "AioAuth ZmYyODk2YTEtYjFkNC00MGY4LWI3YzEtODJkYTZiNzQ1MGZm");
        builder.log(LogDetail.METHOD).log(LogDetail.URI);
        defaultRequestSpec = builder.build();
    }
    private AIOHelper() { }

    RequestSpecification getReqSpec() {
        return defaultRequestSpec;
    }

    public static AIOHelper get() {
        if(aioHelper == null) {
            aioHelper = new AIOHelper();
        }
        return aioHelper;
    }

    public  String findIssueStatus(String projectKey, String issueId) {
        System.out.println("Getting traceability for " + issueId);
        Response response = this.doGet(AIO_URLS.GET_TRACEABILITY, projectKey, issueId);
        List<String> failedResults = response.jsonPath().getList("findAll{testCase -> testCase.testRuns[0].testRunStatus.name == \"Failed\"}");
        System.out.println(failedResults);
        if(failedResults.size() > 0) {
            return "Failed";
        } else {
            return "Passed";
        }
    }

    public void markCaseStatus(String projectKey, String cycleKey, String caseKey, String runStatus, Long duration, boolean addNewRun) {
        Map<String, Object> runStatusPostBody = this.getRunStatus(runStatus, duration);
        Response response = AIOHelper.get().doPost(AIO_URLS.MARK_CASE, runStatusPostBody, projectKey, cycleKey, caseKey );
//        response.prettyPrint();
    }

    private Map<String, Object> getRunStatus(String status, Long duration) {
        Map<String, Object> runStatus = new HashMap<>();
        try {
            runStatus.put("testRunStatus", STATUS_MAP.valueOf(status).getAioMappingStatus());
        } catch (IllegalArgumentException e) {
            runStatus.put("testRunStatus", "Not Run");
        }
        runStatus.put("effort", duration);
        return runStatus;
    }

    private enum STATUS_MAP {
        PASSED("Passed"),
        FAILED("Failed"),
        SKIPPED("Not Run"),
        PENDING("In Progress"),
        UNDEFINED("Not Run"),
        AMBIGUOUS("Not Run");
        String aioMappingStatus;

        STATUS_MAP(String aioMappingStatus) {
            this.aioMappingStatus = aioMappingStatus;
        }

        public String getAioMappingStatus() {
            return aioMappingStatus;
        }

    }

}
