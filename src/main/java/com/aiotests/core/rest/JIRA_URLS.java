package com.aiotests.core.rest;

import io.restassured.http.Method;

public class JIRA_URLS {

    public static final RestAPIDetail GET_ISSUE = new RestAPIDetail("issue/{issueKey}");
    public static final RestAPIDetail TRANSITION_TO_INPROGRESS = new RestAPIDetail("issue/{issueIdOrKey}/transitions","{\"transition\":\"21\"}");
    public static final RestAPIDetail CLOSE_ISSUE = new RestAPIDetail("issue/{issueIdOrKey}/transitions","{\"transition\":\"31\"}");

}
