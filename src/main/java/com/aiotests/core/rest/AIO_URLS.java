package com.aiotests.core.rest;

import io.restassured.http.Method;

public class AIO_URLS {

    public static final RestAPIDetail GET_CYCLE_DETAILS = new RestAPIDetail("/project/{projectKey}/testcycle/{cycleKey}/detail");
    public static final RestAPIDetail GET_CYCLE_SUMMARY = new RestAPIDetail("/project/{projectKey}/testcycle/{cycleKey}/summary");
    public static final RestAPIDetail CREATE_CYCLE = new RestAPIDetail(Method.POST,"/project/{projectKey}/testcycle/detail");
    public static final RestAPIDetail MARK_CASE = new RestAPIDetail(Method.POST,"project/{projectKey}/testcycle/{cycleKey}/testcase/{caseKey}/testrun?createNewRun=true");
    public static final RestAPIDetail GET_TRACEABILITY = new RestAPIDetail("project/{projectKey}/traceability/requirement/{requirementId}?maxRunCount=1");
}
