package com.aiotests.core.rest;
import io.restassured.http.Method;

public class RestAPIDetail {

    private Method method;
    private String path;
    private String body;

    public RestAPIDetail(String path) {
        this.method = Method.GET;
        this.path = path;
    }

    public RestAPIDetail(Method method, String path) {
        this.method = method;
        this.path = path;
    }

    public RestAPIDetail( String path, String body) {
        this.method = Method.POST;
        this.path = path;
        this.body = body;
    }

    public RestAPIDetail(Method method, String path, String body) {
        this.method = method;
        this.path = path;
        this.body = body;
    }

    public Method getMethod() {
        return method;
    }

    public String getPath() {
        return path;
    }

    public String getBody() {
        return body;
    }
}
