package com.aiotests.core.cucumber;

import com.aiotests.core.rest.AIO_URLS;
import com.aiotests.core.rest.AIOHelper;
import com.aiotests.core.rest.JIRA_URLS;
import com.aiotests.core.rest.JiraHelper;
import io.cucumber.plugin.ConcurrentEventListener;
import io.cucumber.plugin.event.*;
import io.restassured.response.Response;
import org.testng.log4testng.Logger;

import java.util.*;

public class AIOCucumberEventListener implements ConcurrentEventListener {

    Logger logger = Logger.getLogger(AIOCucumberEventListener.class);
    String cycleKey = System.getProperty("aioCycleKey","NVPROJ-CY-72");
    String jiraStory = "NVPROJ-24";
    Set<String> jiraStories = new HashSet<>();
    String projectKey = "NVPROJ";
    List<String> lstOfFailedUpdates = new ArrayList<>();
    public AIOCucumberEventListener() {
    }

    @Override
    public void setEventPublisher(EventPublisher publisher) {
        publisher.registerHandlerFor(TestRunStarted.class, this::handleTestRunStarted);
//        publisher.registerHandlerFor(TestCaseStarted.class, this::handleTestCaseStarted);
        publisher.registerHandlerFor(TestCaseFinished.class, this::handleTestCaseFinished);
        publisher.registerHandlerFor(TestRunFinished.class, this::finishReport);
    }

    private void handleTestRunStarted(TestRunStarted testRunStarted) {
        Map<String, Object> cycleDetails = new HashMap<>();
        cycleDetails.put("title", "Test Cycle automation");
        cycleDetails.put("objective", "Trial Run");
    }

    private  void handleTestCaseFinished(TestCaseFinished testCaseFinishedEvent) {
        System.out.println("Test Case finished: " + testCaseFinishedEvent.getResult());

        List<String> scenarioTags = testCaseFinishedEvent.getTestCase().getTags();
        if(scenarioTags.size() > 0) {
            scenarioTags.forEach(t -> {
                if(t.startsWith("@"+ this.projectKey + "-TC-")){
                    String status = testCaseFinishedEvent.getResult().getStatus().name();
                    Long duration = testCaseFinishedEvent.getResult().getDuration().getSeconds();
                    AIOHelper.get().markCaseStatus(projectKey, cycleKey, t.substring(1), status, duration, true);
                } else if (t.startsWith("@"+ this.projectKey + "-")) {
                    jiraStories.add(t.substring(1));
                }
            });
        }
    }

    private void finishReport(TestRunFinished testRunFinished) {
        System.out.println("Test Run finished: " + testRunFinished.getResult());
        System.out.println("Stories found " + jiraStories);
        for (String story : jiraStories) {
            String issueId = JiraHelper.get().getIssueId(story);
            if(AIOHelper.get().findIssueStatus(this.projectKey,issueId) == "Failed"){
                JiraHelper.get().moveToInProgress(issueId);
            } else {
                JiraHelper.get().closeIssue(issueId);
            }
        }

    }



}
